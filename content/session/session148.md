---
title: "جلسه ۱۴۸"
date: "1397-04-25"
author: "وجیهه نیکخواه"
draft: false
categories:
    - "sessions"
---
[![poster148](../../img/posters/poster148.jpg)](../../img/poster148.jpg)


فایل های این ارائه را می توانید از 
[اینجا](https://gitlab.com/shirazlug/resources/tree/master/presentations/session_148) 
یا
[اینجا](https://www.slideshare.net/ShirazLUG/shirazlug-session-148) 
دانلود کنید.
